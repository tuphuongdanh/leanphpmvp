# PHP - MVC - DanhTP
## Run on your computer
- Copy .env.example to .env and change the values to yours
- Open your terminal run following command line

``sh
php -S localhost:8000
``
## Run with Docker
- Copy .env.example to .env and change the values to yours
- Open your terminal run following command line

``sh
docker-compose up -d
``
