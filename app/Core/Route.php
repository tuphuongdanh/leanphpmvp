<?php

/**
 * Route class
 */
class Route
{
	
	protected $params = array();

	public function getParams($nameParam = '', $default = null) {
		if (!empty($nameParam)) {
			if (isset($params[$nameParam])) {
				$params[$nameParam];
			}
			if ($default !== null) {
				return $default;
			}
			return null;
		}
		return $this->params;
	}

	public function setParams($nameParam, $value) {
		if (!isset($nameParam) || !isset($value)) {
			throw new Exception("Param name and value are required", 1);	
		}
		$this->params[$nameParam] = $value;
	}
}