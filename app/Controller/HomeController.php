<?php

namespace App\Controller;

use App\Core\Controller;
use App\Model\UserModel;
use App\Model\CityModel;
use Elasticsearch\ClientBuilder;
/**
 * @HomeController default controller, you are able to edit in index.php
 */
class HomeController extends Controller
{
    protected $esClient;
    function __construct() {
        parent::__construct();
        $this->esClient = ClientBuilder::create()->setHosts(['http://localhost:9200'])->build();
    }

	public function index($id = null) {
        echo "Hello World";
        // $userModel = new UserModel();
        // $userModel->getOne();
    }

    public function search() {
        
        echo "Search function";
    }

    public function indexDocument() {
        $params = [
            'index' => 'my_index',
            'id'    => '01',
            'body'  => [
                'title' => 'PHP tutorial',
                'description'  => 'Well organized and easy to understand Web building tutorials with lots of examples of how to use PHP',
                'keywords' => ['php', 'tutorial']
            ]
        ];
        
        $response = $this->esClient->index($params);
        echo '<pre>';
        print_r($response);
    }


    
    public function getItem($id) {
        return $id;
    }
}