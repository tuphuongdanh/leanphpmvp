<?php

namespace App\Test;

use App\Controller\HomeController;
use App\Model\UserModel;
use PHPUnit\Framework\TestCase;

final class HomeTest extends TestCase {

  /**
   * Undocumented function
   * @test
   * @return void
   */
  public function testPushAndPop() {

    $stack = [];
    $this->assertSame(0, count($stack), "count array");

    array_push($stack, 'foo');
    $this->assertSame('foo', $stack[count($stack) - 1]);
    $this->assertSame(1, count($stack));

    $this->assertSame('foo', array_pop($stack));
    $this->assertSame(0, count($stack));
  }

  // public function testGetTtem() {
  //   $homeController = new HomeController;
  //   $this->assertSame('1', $homeController->getItem('1'));
  // }

  public function testGetOne() {
    $userModel = new UserModel;
    $this->assertSame('1', $userModel->get('1'));
  }
}