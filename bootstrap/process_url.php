<?php

use App\Repository\UserRepository;

/**
 * ProcessUrl
 */
class ProcessUrl
{
	public static function getEntity() {
		$entity = array(
			'controller' => DEFAULT_COTROLLER,
			'action'	=> 'index'
		);
		$rootUrl = str_replace("https://", "", str_replace("http://", "", APP_PATH));
		$uri = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$url = str_replace($rootUrl, "", $uri);
		$url = trim($url,"/");
		if ($url == '' || substr ($url, 0, 1) == '?') {
			return $entity;
		}
		$urlSplit = explode('?', $url);
		if (count($urlSplit) > 0) {
			$urlExe = explode('/', $urlSplit[0]);
			if (count($urlExe) > 0) {
				$controllerUrl = explode('-', $urlExe[0]);
				$controllerName = implode('', array_map(function ($v) {
					return ucwords($v);
				}, $controllerUrl));
				$entity['controller'] = $controllerName;
			}
			if (count($urlExe) > 1) {
				$actionUrl = explode('-', $urlExe[1]);
				$actionName = implode('', array_map(function ($v) {
					return ucwords($v);
				}, $actionUrl));
				$entity['action'] = lcfirst($actionName);
			}
		}
		return $entity;
	}

	public function exeUrl() {
		$entity = self::getEntity();
		$controllerName = "App\Controller\\".$entity['controller']."Controller";
		
		try {
			// $reflector = new \ReflectionClass($controllerName);
			// $constructor = $reflector->getConstructor();
			// if ($constructor && $constructor->getParameters()) {
			// 	$docComment = $constructor->getDocComment();
			// 	echo '<pre>';
			// 	var_dump($docComment);
			// 	print_r($constructor->getParameters());
			// 	exit;
			// 		// $instance = $reflector->newInstanceArgs($sub_page['data']);
			// } else {
			// 		// $instance = new SomeClass;
			// }
			$controller = new $controllerName(new UserRepository);
			$controller->{$entity['action']}();
		} catch (Throwable $t) {
			echo $this->formatError($t);
		}
	}

	protected function formatError(Throwable $err) {
		$str = '<div style="background-color: #cdcdcd; padding: 20; border: 1px solid #888888">';
		$str .= '<p><b>Message:</b> '.$err->getMessage().'</p>';
		$str .= '<p><b>File:</b> '.$err->getFile().'</p>';
		$str .= '<p><b>Line:</b> '.$err->getLine().'</p>';
		$str .= '</div>';
		return $str;
	}
}