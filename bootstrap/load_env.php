<?php

function load(string $path) {
  $envFile = fopen("$path/.env", "r") or die(".env file is not found");
  $envStr = fread($envFile,filesize("$path/.env"));
  fclose($envFile);
  $arrEnv = explode("\n", $envStr);
  if (count($arrEnv) > 0) {
    foreach($arrEnv as $str) {
      putenv($str);
    } 
  }
}
